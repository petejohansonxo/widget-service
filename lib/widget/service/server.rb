require 'widget_service_services_pb'
require 'widget/widget'

class Widget::Service::Server < WidgetService::Service
  def first_widget(req, _call)
    name = Widget::Widget.arel_table[:name]
    match = Widget::Widget.where(name.matches("#{req.name_match}%")).first
    if match.present?
      WidgetMessage.new(name: match.name)
    else
      fail GRPC::NotFound.new("No widget found matching that name")
    end
  end

  def first_widget_insecure(req, _call)
    match = Widget::Widget.where("name LIKE '#{req.name_match}%'").first
    if match.present?
      WidgetMessage.new(name: match.name)
    else
      fail GRPC::NotFound.new("No widget found matching that name")
    end
  end
end
