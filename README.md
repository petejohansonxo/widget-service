# WidgetService

## Usage

Before beggining, create a `db/config.yml` file with your Postgres database settings, e.g.:

```yaml
development:
  database: widget_service_db
  user: petejohanson
  adapter: postgresql
```

Run the following commands to exercise the software:

```bash
$ bundle
$ bundle exec rake bootstrap
$ ./bin/server
```

Then in another terminal

```bash
$ ./bin/client
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

