
lib = File.expand_path('../../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require "bundler/setup"
require "widget/widget"

Widget::Widget.create! name: "Testing"
Widget::Widget.create! name: "1, 2, 3"
